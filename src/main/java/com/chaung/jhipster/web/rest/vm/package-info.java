/**
 * View Models used by Spring MVC REST controllers.
 */
package com.chaung.jhipster.web.rest.vm;
